<?php

namespace App\Http\Requests\Product;

use App\Enums\ProductStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'price' => ['nullable', 'numeric', 'min:1', 'max:100000'],
            'count' => ['nullable', 'numeric', 'min:0', 'max:1000'],
            'status' => ['nullable', new Enum(type: ProductStatus::class)],
        ];
    }
}
