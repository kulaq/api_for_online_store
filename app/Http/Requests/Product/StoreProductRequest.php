<?php

namespace App\Http\Requests\Product;

use App\Enums\ProductStatus;
use App\Services\Product\DTO\CreateProductDTO;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['string'],
            'price' => ['required', 'numeric', 'min:1', 'max:100000'],
            'count' => ['required', 'numeric', 'min:0', 'max:1000'],
            'status' => ['required', new Enum(type: ProductStatus::class)],
            'images.*' => ['image'],
        ];
    }

    public function data(): CreateProductDTO
    {
        return CreateProductDTO::from($this->validated());
    }
}
