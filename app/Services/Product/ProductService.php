<?php

namespace App\Services\Product;

use App\Enums\ProductStatus;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\StoreReviewRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Models\Product;
use App\Services\Product\DTO\CreateProductDTO;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    private Product $product;

    public function published(array $fields = ['id', 'name', 'price']): Collection
    {
        return Product::query()
            ->select($fields)
            ->whereStatus(ProductStatus::Published)
            ->get();
    }

    public function store(CreateProductDTO $data): Product
    {
        $images = Arr::get($data->toArray(), 'images');

        /** @var Product $product */
        $product = auth()->user()->products()->create(
            $data->except('images')->toArray()
        );

        if ($images !== null) {
            foreach ($images as $image) {
                $path = $image->storePublicly('images');

                $product->images()->create([
                    'url' => config('app.url').Storage::url($path),
                ]);
            }
        }

        return $product;
    }

    public function update(UpdateProductRequest $request): Product
    {
        if ($request->method() === 'PUT') {
            $this->product->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
                'count' => $request->input('count'),
                'status' => $request->enum('status', ProductStatus::class),
            ]);
        } else {
            //TODO использовать DTO
            $data = [];

            if ($request->has('name')) {
                $data['name'] = $request->input('name');
            }

            if ($request->has('description')) {
                $data['description'] = $request->input('description');
            }

            if ($request->has('price')) {
                $data['price'] = $request->input('price');
            }

            if ($request->has('count')) {
                $data['count'] = $request->input('count');
            }

            if ($request->has('status')) {
                $data['status'] = $request->input('status');
            }

            $this->product->update($data);
        }
        return $this->product;
    }

    public function addReview(StoreReviewRequest $request)
    {
        return $this->product->reviews()->create([
            'user_id' => auth()->id(),
            'text' => $request->str('text'),
            'rating' => $request->integer('rating'),
        ]);
    }

    public function setProduct(Product $product): ProductService
    {
        $this->product = $product;
        return $this;
    }

}
