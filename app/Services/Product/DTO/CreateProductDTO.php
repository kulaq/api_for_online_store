<?php

namespace App\Services\Product\DTO;

use App\Enums\ProductStatus;
use Spatie\LaravelData\Data;

class CreateProductDTO extends Data
{
    public function __construct(
        public string $name,

        public ?string $description,

        public int|float $price,

        public int $count,

        public ProductStatus $status,

        public ?array $images,
    ) {}

}
